package cat.itb.mainwhatsapp.Model;

import android.graphics.Bitmap;

import java.util.Date;


public class Call {
    private String name;
    private boolean send;
    private Bitmap image;
    private String type;
    private String time;

    public Call(String name, boolean send, Bitmap image, String type, String time) {
        this.name = name;
        this.send = send;
        this.image = image;
        this.type = type;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSend() {
        return send;
    }

    public void setSend(boolean send) {
        this.send = send;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}