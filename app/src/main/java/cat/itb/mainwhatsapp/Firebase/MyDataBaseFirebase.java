package cat.itb.mainwhatsapp.Firebase;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;

import cat.itb.mainwhatsapp.Model.User;

public class MyDataBaseFirebase {

    private final FirebaseStorage myStorage;
    private final FirebaseFirestore db;

    public MyDataBaseFirebase() {
        db = FirebaseFirestore.getInstance();
        myStorage = FirebaseStorage.getInstance();
    }

    public Query getCollection(String ref) {
        return db.collection(ref);
    }

    /**
     * Agregar usuario
     *
     * @param user
     * @param listener
     */
    public void uploadUser(User user, final OnUserUpdated listener) {
        db.collection("Users")
                .document(user.getId())
                .set(user)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        listener.onSuccess(task.isSuccessful());
                    }
                });
    }

    /**
     * Carga una imagen en la view seleccionada, según nombre i carpeta seleccioonada
     *
     * @param uri     nombre de la imagen
     * @param carpeta carpeta contenedora
     * @param v       view donde sera cargada la imagen
     * @param context contexto
     */
    @SuppressLint("CheckResult")
    public void getImage(String uri, String carpeta, final View v, Context context) {
        myStorage.getReference(carpeta).child(uri).getDownloadUrl().addOnSuccessListener(uri1 -> {
            RequestOptions options = new RequestOptions();
            options.centerCrop();
            Glide.with(context)
                    .load(uri1)
                    .apply(options)
                    .into((ShapeableImageView) v);
        });
    }

    /**
     * Subir imagen
     *
     * @param name    nombre de la imagen
     * @param carpeta carpeta contenedora
     * @param uri     dirrecion
     */
    public void uploadImage(String name, String carpeta, Uri uri) {
        UploadTask uploadTask = myStorage.getReference(carpeta).child(name).putFile(uri);
    }

    public Query getContactQuery(String userid) {
        return db.collection("Users").whereNotEqualTo("id", userid);
    }

    public interface OnUserUpdated {
        void onSuccess(boolean isUpload);
    }
}
