package cat.itb.mainwhatsapp.Firebase;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MyAuthFirebase {

    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;

    public MyAuthFirebase() {
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
    }

    public FirebaseUser getFirebaseUser() {
        return firebaseUser;
    }

    public void registerNewUser(String email, String pass, final OnRegisterSuccess listener) {
        firebaseAuth.createUserWithEmailAndPassword(email, pass)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        reloadAuth();
                        listener.onSuccess(task.isSuccessful());
                    }
                });
    }

    public void signInUser(String email, String pass, final OnSignInSuccess listener) {
        firebaseAuth.signInWithEmailAndPassword(email, pass).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                reloadAuth();
                listener.onSuccess(true);
            }
        });
    }

    public void sendMailVerification() {
        firebaseUser.sendEmailVerification()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        System.out.println("Email sended");
                    }
                });
    }

    public boolean VerifiedEmail() {
        return firebaseUser.isEmailVerified();
    }

    public void reloadAuth() {
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
    }

    public void checkEmailExists(String email, final OnEmailCheckListener listener) {
        firebaseAuth.fetchSignInMethodsForEmail(email).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {

                boolean check = task.getResult().getSignInMethods().isEmpty();

                listener.onSuccess(check);
            }
        });
    }

    public interface OnRegisterSuccess {
        void onSuccess(boolean isRegister);
    }

    public interface OnEmailCheckListener {
        void onSuccess(boolean isNew);
    }

    public interface OnSignInSuccess {
        void onSuccess(boolean isSuccess);
    }


}

