package cat.itb.mainwhatsapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.imageview.ShapeableImageView;

import java.util.List;

import cat.itb.mainwhatsapp.Fragments.main.DialogViewUser;
import cat.itb.mainwhatsapp.Fragments.main.FragmentHomeDirections;
import cat.itb.mainwhatsapp.Model.User;
import cat.itb.mainwhatsapp.R;

public class MainContactsAdapter extends RecyclerView.Adapter<MainContactsAdapter.ContactViewHolder> {
    List<User> userList;
    Context context;

    public MainContactsAdapter(List<User> userList, Context context) {
        this.userList = userList;
        this.context = context;
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vh = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_contact, parent, false);
        return new ContactViewHolder(vh);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {
        holder.bindData(userList.get(position));
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    class ContactViewHolder extends RecyclerView.ViewHolder{

        ShapeableImageView imagePerfile;
        TextView textViewName, textViewTime;

        public ContactViewHolder(@NonNull View itemView) {
            super(itemView);

            imagePerfile = itemView.findViewById(R.id.imagePerfileContactRecyclerView);
            textViewName = itemView.findViewById(R.id.textViewNameContactRecyclerView);
            textViewTime = itemView.findViewById(R.id.textViewDateContactRecyclerView);

            itemView.setOnClickListener(v -> {
                NavDirections navDirections = FragmentHomeDirections.actionFragmentHomeToFragmentChat(userList.get(getAdapterPosition()));
                Navigation.findNavController(v).navigate(navDirections);
            });

            imagePerfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new DialogViewUser(context, userList.get(getAdapterPosition()));
                }
            });
        }

        public void bindData(User user){
            //imagePerfile.setImageBitmap(user.getImage());
            textViewName.setText(user.getName());
            //textViewTime.setText(user.getHora());
        }
    }

}
