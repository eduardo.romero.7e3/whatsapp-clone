package cat.itb.mainwhatsapp.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.imageview.ShapeableImageView;

import java.util.List;

import cat.itb.mainwhatsapp.Fragments.main.ContactFragmentDirections;
import cat.itb.mainwhatsapp.Model.User;
import cat.itb.mainwhatsapp.R;

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolderContact> {
    List<User> userList;

    public ContactListAdapter(List<User> userList) {
        this.userList = userList;
    }

    @NonNull
    @Override
    public ViewHolderContact onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vh = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_contact, parent, false);
        return new ViewHolderContact(vh);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderContact holder, int position) {
        holder.bindData(userList.get(position));
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    class ViewHolderContact extends RecyclerView.ViewHolder{

        ShapeableImageView imagePerfile;
        TextView textViewName;

        public ViewHolderContact(@NonNull View itemView) {
            super(itemView);
            imagePerfile = itemView.findViewById(R.id.imagePerfileContactMainRecyclerView);
            textViewName = itemView.findViewById(R.id.textViewNameContactMainRecyclerView);

            itemView.setOnClickListener(v -> {
                NavDirections navDirections = ContactFragmentDirections.actionContactFragmentToFragmentChat(userList.get(getAdapterPosition()));
                Navigation.findNavController(v).navigate(navDirections);
            });
        }

        public void bindData(User user){
            //imagePerfile.setImageBitmap(user.getImage());
            textViewName.setText(user.getName());
        }
    }
}
