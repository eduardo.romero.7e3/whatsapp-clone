package cat.itb.mainwhatsapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.ObservableSnapshotArray;
import com.google.android.material.imageview.ShapeableImageView;

import cat.itb.mainwhatsapp.Fragments.main.ContactFragmentDirections;
import cat.itb.mainwhatsapp.Model.User;
import cat.itb.mainwhatsapp.R;
import cat.itb.mainwhatsapp.Firebase.MyDataBaseFirebase;

public class ContactListFirebaseAdapter extends FirebaseRecyclerAdapter<User, ContactListFirebaseAdapter.ViewHolderContact> {

    MyDataBaseFirebase db;
    Context context;
    String userId;

    public ContactListFirebaseAdapter(@NonNull FirebaseRecyclerOptions<User> options, MyDataBaseFirebase db, Context context, String userId) {
        super(options);
        this.db = db;
        this.context = context;
        this.userId = userId;
    }

    @NonNull
    @Override
    public ViewHolderContact onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vh = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_contact, parent, false);
        return new ViewHolderContact(vh);
    }

    @NonNull
    @Override
    public ObservableSnapshotArray<User> getSnapshots() {
        return super.getSnapshots();
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolderContact holder, int position, @NonNull User model) {
            holder.bindData(model);
    }

    class ViewHolderContact extends RecyclerView.ViewHolder {

        ShapeableImageView imagePerfile;
        TextView textViewName;

        public ViewHolderContact(@NonNull View itemView) {
            super(itemView);
            imagePerfile = itemView.findViewById(R.id.imagePerfileContactMainRecyclerView);
            textViewName = itemView.findViewById(R.id.textViewNameContactMainRecyclerView);
        }

        public void bindData(User user) {
                db.getImage(user.getImage(), "/Users", imagePerfile, context);
                textViewName.setText(user.getName());

                itemView.setOnClickListener(v -> {
                    NavDirections navDirections = ContactFragmentDirections.actionContactFragmentToFragmentChat(user);
                    Navigation.findNavController(v).navigate(navDirections);
                });
        }
    }
}
