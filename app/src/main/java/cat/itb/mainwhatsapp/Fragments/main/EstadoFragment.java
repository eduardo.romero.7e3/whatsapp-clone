package cat.itb.mainwhatsapp.Fragments.main;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.progressindicator.LinearProgressIndicator;

import cat.itb.mainwhatsapp.Model.Estado;
import cat.itb.mainwhatsapp.R;

public class EstadoFragment extends Fragment implements View.OnTouchListener {

    Estado estado;
    ShapeableImageView imageEstado;
    MaterialToolbar estadoToolBar;

    TextView namePorfile;
    ShapeableImageView imagePorfile;
    LinearProgressIndicator mProgressBar;
    ObjectAnimator mAnimation;

    public EstadoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_status, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(getArguments() != null) estado = getArguments().getParcelable("Estado");

        imageEstado = view.findViewById(R.id.imageEstadoFragment);
        estadoToolBar = view.findViewById(R.id.topAppBarEstadoFragment);
        namePorfile = view.findViewById(R.id.nameContactEstadoFragment);
        imagePorfile = view.findViewById(R.id.profile_image_estado_fragment);
        mProgressBar = view.findViewById(R.id.progress_bar_estado_fragment);

        imageEstado.setImageBitmap(estado.getImatge());

        imagePorfile.setImageBitmap(estado.getUserImage());
        namePorfile.setText(estado.getUserid());


        estadoToolBar.setNavigationOnClickListener(v -> mAnimation.end());

        imageEstado.setOnTouchListener(this::onTouch);

        setProgressAnimate(view);

    }

    private void setProgressAnimate(View v) {
        mAnimation = ObjectAnimator.ofInt(mProgressBar, "progress", 0, 100);
        mAnimation.setDuration(5000); // 5 seconds
        mAnimation.setInterpolator(new DecelerateInterpolator());
        mAnimation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                mAnimation.cancel();
                Navigation.findNavController(v).popBackStack();
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });

        mAnimation.start();
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                mAnimation.pause();
                break;
            case MotionEvent.ACTION_UP:
                mAnimation.resume();
                break;
        }
        return true;
    }
}