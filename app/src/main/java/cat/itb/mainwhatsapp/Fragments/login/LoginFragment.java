package cat.itb.mainwhatsapp.Fragments.login;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import cat.itb.mainwhatsapp.R;
import cat.itb.mainwhatsapp.Firebase.MyAuthFirebase;

public class LoginFragment extends Fragment {

    private EditText editTextPassword;
    private EditText editTextEmail;
    private Button buttonNext;

    MyAuthFirebase mAuth;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = new MyAuthFirebase();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        editTextEmail = view.findViewById(R.id.editText_email_login_fragment);
        editTextPassword = view.findViewById(R.id.editText_pass_login_fragment);
        buttonNext = view.findViewById(R.id.button_next_login_fragment);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());



        if(mAuth.getFirebaseUser() != null){
            Navigation.findNavController(getView()).navigate(R.id.action_loginFragment_to_fragmentHome);
        }


        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = editTextEmail.getText().toString();
                String pass = editTextPassword.getText().toString();

                mAuth.checkEmailExists(email, new MyAuthFirebase.OnEmailCheckListener() {
                    @Override
                    public void onSuccess(boolean isNew) {
                        if(isNew){
                            builder.setMessage("We will be verify the email address:\n"+ email+"\n Is this OK, or would you like to edit the address?");

                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    mAuth.registerNewUser(email, pass, new MyAuthFirebase.OnRegisterSuccess() {
                                        @Override
                                        public void onSuccess(boolean isRegister) {
                                            if(isRegister){
                                                Navigation.findNavController(v).navigate(R.id.action_loginFragment_to_verifyFragment);
                                            }
                                        }
                                    });

                                }
                            });

                            builder.setNegativeButton("Edit", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });

                            builder.show();
                        }else {
                            mAuth.signInUser(email, pass, new MyAuthFirebase.OnSignInSuccess() {
                                @Override
                                public void onSuccess(boolean isSuccess) {
                                    if(isSuccess){
                                        Navigation.findNavController(v).navigate(R.id.action_loginFragment_to_fragmentHome);
                                    }else {
                                        editTextEmail.setError("Wrong Password.");
                                    }
                                }
                            });
                        }
                    }
                });

                //mAuth.checkEmailExists(editTextEmail.getText().toString());


            }
        });

    }

}