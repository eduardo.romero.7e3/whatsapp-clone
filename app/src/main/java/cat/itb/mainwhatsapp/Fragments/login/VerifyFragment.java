package cat.itb.mainwhatsapp.Fragments.login;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import cat.itb.mainwhatsapp.Model.User;
import cat.itb.mainwhatsapp.R;
import cat.itb.mainwhatsapp.Firebase.MyAuthFirebase;

public class VerifyFragment extends Fragment {

    private User user;

    MyAuthFirebase mAuth;

    LinearLayout buttonRecent;

    public VerifyFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_verify, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        if(mAuth.VerifiedEmail()){
            Navigation.findNavController(getView()).navigate(R.id.action_verifyFragment_to_infoLoginFragment);
        }else {
            Navigation.findNavController(getView()).navigate(R.id.action_verifyFragment_to_infoLoginFragment);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        buttonRecent = view.findViewById(R.id.layout_recent_mail_verify_fragment);

        mAuth = new MyAuthFirebase();

        mAuth.sendMailVerification();

        Toast.makeText(getContext(), mAuth.getFirebaseUser().getEmail(), Toast.LENGTH_LONG).show();

        buttonRecent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.sendMailVerification();
            }
        });
    }
}
