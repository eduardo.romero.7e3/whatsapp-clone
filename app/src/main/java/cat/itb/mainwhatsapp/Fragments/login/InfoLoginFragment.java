package cat.itb.mainwhatsapp.Fragments.login;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.imageview.ShapeableImageView;

import java.util.Objects;

import cat.itb.mainwhatsapp.Model.User;
import cat.itb.mainwhatsapp.R;
import cat.itb.mainwhatsapp.Firebase.MyAuthFirebase;
import cat.itb.mainwhatsapp.Firebase.MyDataBaseFirebase;

public class InfoLoginFragment extends Fragment {

    private static final int PICK_IMAGE = 1;

    ShapeableImageView imageView;
    EditText editTextName;
    Button button;

    BottomSheetDialog bottomSheetCamera;

    MyDataBaseFirebase myDb;
    MyAuthFirebase mAuth;

    Uri imageUri;
    boolean imageSet = false;

    ActivityResultLauncher<Intent> someActivityResultLauncher;

    public InfoLoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_info_login, container, false);


        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAuth = new MyAuthFirebase();
        myDb = new MyDataBaseFirebase();

        imageView = view.findViewById(R.id.shapeableImageView_info_login_fragment);
        editTextName = view.findViewById(R.id.editTextUsername_info_login_fragment);
        button = view.findViewById(R.id.button_next_info_login_fragment);

        activityResult();

        bottomSheetCamera = new BottomSheetDialog(requireContext());

        if(imageUri != null){
            imageView.setImageURI(imageUri);
        }

        imageView.setOnClickListener(v12 -> showBottomDialogPick());

        button.setOnClickListener(v -> {

            myDb.uploadImage(mAuth.getFirebaseUser().getUid()+"image", "/Users", imageUri);
            User user = new User(mAuth.getFirebaseUser().getUid(), editTextName.getText().toString(), "default");
            if(imageSet){
                user.setImage(mAuth.getFirebaseUser().getUid()+"image");
            }
            myDb.uploadUser(user, isUpload -> {
                if(isUpload){
                    Navigation.findNavController(getView()).navigate(R.id.action_infoLoginFragment_to_fragmentHome);
                }
            });
        });



    }

    private void showBottomDialogPick() {
        View view = getLayoutInflater().inflate(R.layout.settings_profile_camera_options, null);

        bottomSheetCamera.setContentView(view);

        Objects.requireNonNull(bottomSheetCamera.getWindow()).addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        bottomSheetCamera.show();

        bottomSheetCamera.setOnDismissListener(dialog -> bottomSheetCamera.cancel());

        view.findViewById(R.id.bottomDialogOption_camera).setOnClickListener(v -> openCamera());

        view.findViewById(R.id.bottomSheetCameraGalleryOption).setOnClickListener(v -> openGallery());

        view.findViewById(R.id.bottomSheetCameraRemoveOption).setOnClickListener(v -> {
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_stat_name));
            imageView.setScaleType(ImageView.ScaleType.CENTER);
            bottomSheetCamera.cancel();
        });
    }

    private void openCamera() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        startActivity(intent);
    }

    private void openGallery(){
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        someActivityResultLauncher.launch(gallery);
    }

    private void activityResult(){
        someActivityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        // There are no request codes
                        Intent data = result.getData();
                        imageUri = data.getData();
                        imageView.setImageURI(imageUri);
                        imageView.setImageTintList(null);
                        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                        imageSet = true;
                        bottomSheetCamera.cancel();
                    }
                });
    }
}