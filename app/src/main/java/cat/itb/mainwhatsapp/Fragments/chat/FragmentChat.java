package cat.itb.mainwhatsapp.Fragments.chat;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.textfield.TextInputEditText;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import cat.itb.mainwhatsapp.Adapter.MessageAdapter;
import cat.itb.mainwhatsapp.Model.MessageModel;
import cat.itb.mainwhatsapp.Model.User;
import cat.itb.mainwhatsapp.R;

public class FragmentChat extends Fragment {

    public static final int show_contact = 1000418;
    public static final int search = 1000047;
    public static final int empty_chat = 1000638;

    List<MessageModel> messageModelList;
    String username;
    User user;
    ImageView userImageView;
    TextView userNameView, userTimeStampView;
    RecyclerView recyclerView;
    MessageAdapter adapter;
    Button attachButton;
    ImageButton sendButton;
    MaterialToolbar toolbar;
    TextInputEditText input;
    LinearLayoutManager layoutManager;
    LinearLayout navButton;

    public FragmentChat() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.chat_top_app_bar, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        toolbar = view.findViewById(R.id.topAppBar);
        attachButton = view.findViewById(R.id.btn_attach);
        input = view.findViewById(R.id.chat_text_input);
        sendButton = view.findViewById(R.id.btn_send);
        userNameView = view.findViewById(R.id.profile_name);
        userImageView = view.findViewById(R.id.profile_image);
        userTimeStampView = view.findViewById(R.id.profile_timestamp);
        navButton = view.findViewById(R.id.navigation_button_chat_top_app_bar);

        if (getArguments() != null) {
            user = getArguments().getParcelable("userToChat");
            userNameView.setText(user.getName());
            //userImageView.setImageBitmap(user.getImage());
        }

        attachButton.setOnClickListener(v -> openDialog());

        toolbar.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case show_contact:
                    NavDirections navDirections = FragmentChatDirections.actionFragmentChatToFragmentChatProfile(user);
                    Navigation.findNavController(view).navigate(navDirections);
                    break;
                case search:
                case R.id.change_background:
                    Toast.makeText(getContext(), "Coming soon!", Toast.LENGTH_SHORT).show();
                    break;
                case empty_chat:
                    messageModelList.clear();
                    adapter.setMessageList(messageModelList);
                    break;
            }
            return true;
        });

        navButton.setOnClickListener(v -> Navigation.findNavController(v).popBackStack());

        sendButton.setOnClickListener(v -> {
            if (!input.getText().toString().isEmpty()) {

                Date date = new Date();   // given date
                Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
                calendar.setTime(date);   // assigns calendar to given date
                String s = LocalDateTime.now().getHour() + ":";
                if (LocalDateTime.now().getMinute() / 10 == 0) s += '0';
                s += LocalDateTime.now().getMinute();

                messageModelList.add(new MessageModel(username, input.getText().toString(), s, true));
                adapter.setMessageList(messageModelList);
                input.setText("");
                layoutManager.scrollToPosition(messageModelList.size() - 1);

            }
        });

        username = "Pere";
        messageModelList = new ArrayList<>();
        messageModelList.add(new MessageModel("Marta", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "11:25", false));
        messageModelList.add(new MessageModel("Pere", "AAAAAAAAAAAAAAAAAAA", "11:26", true));
        messageModelList.add(new MessageModel("Marta", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "11:25", false));
        messageModelList.add(new MessageModel("Pere", "AAAAAAAAAAAAAAAAAAA", "11:26", true));
        messageModelList.add(new MessageModel("Marta", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "11:25", false));
        messageModelList.add(new MessageModel("Pere", "AAAAAAAAAAAAAAAAAAA", "11:26", true));
        messageModelList.add(new MessageModel("Marta", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "11:25", false));
        messageModelList.add(new MessageModel("Pere", "AAAAAAAAAAAAAAAAAAA", "11:26", true));
        messageModelList.add(new MessageModel("Marta", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "11:25", false));
        messageModelList.add(new MessageModel("Pere", "AAAAAAAAAAAAAAAAAAA", "11:26", true));
        messageModelList.add(new MessageModel("Marta", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "11:25", false));
        messageModelList.add(new MessageModel("Pere", "AAAAAAAAAAAAAAAAAAA", "11:26", true));
        messageModelList.add(new MessageModel("Marta", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "11:25", false));
        messageModelList.add(new MessageModel("Pere", "A", "11:26", true));
        messageModelList.add(new MessageModel("Marta", "dfhs", "11:25", false));
        messageModelList.add(new MessageModel("Pere", "AAAAAAAAAAAAAAAAAAA", "11:26", true));
        messageModelList.add(new MessageModel("Marta", "EEEEEEYAWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW", "11:25", false));
        messageModelList.add(new MessageModel("Pere", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "11:26", true));
        messageModelList.add(new MessageModel("Marta", "AWAAAAAAAWWWWWWWWWWWWWWWWWWW", "11:25", false));
        messageModelList.add(new MessageModel("Pere", "AAAAAAAAAAAAAAAAAAA", "11:26", true));
        messageModelList.add(new MessageModel("Marta", "WOOOOOOOOOOOO", "11:25", false));
        messageModelList.add(new MessageModel("Pere", "AAAAAAA", "11:26", true));


        recyclerView = view.findViewById(R.id.message_recyclerview);
        adapter = new MessageAdapter(messageModelList);
        recyclerView.setAdapter(adapter);
        layoutManager = new LinearLayoutManager(getContext());
        layoutManager.getStackFromEnd();
        recyclerView.setLayoutManager(layoutManager);
        adapter.setMessageList(messageModelList);
        layoutManager.scrollToPosition(messageModelList.size() - 1);
    }

    private void openDialog() {
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View inflatedView;

        inflatedView = layoutInflater.inflate(R.layout.chat_attach_menu_layout, null, false);

        LinearLayout layoutGallery;
        layoutGallery = inflatedView.findViewById(R.id.attachmentGalleryLayout);
        layoutGallery.setOnClickListener(view -> FloatingView.dismissWindow());

        FloatingView.onShowPopup(getActivity(), inflatedView);
    }
}