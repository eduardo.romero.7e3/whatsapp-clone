package cat.itb.mainwhatsapp.Fragments.main;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.appbar.MaterialToolbar;

import java.lang.reflect.Field;
import java.util.ArrayList;

import cat.itb.mainwhatsapp.Adapter.ContactListFireStoreAdapter;
import cat.itb.mainwhatsapp.Model.User;
import cat.itb.mainwhatsapp.R;
import cat.itb.mainwhatsapp.Firebase.MyAuthFirebase;
import cat.itb.mainwhatsapp.Firebase.MyDataBaseFirebase;


public class ContactFragment extends Fragment {

    RecyclerView recyclerView;
    MaterialToolbar mainMaterialToolbar, searchToolbar;

    MyDataBaseFirebase db;
    MyAuthFirebase myAuthFirebase;

    SearchView searchView;
    ContactListFireStoreAdapter adapter;

    Menu search_menu;
    MenuItem item_search;

    public ContactFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contact, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ArrayList<User> userList = new ArrayList<User>();

        db = new MyDataBaseFirebase();
        myAuthFirebase = new MyAuthFirebase();

        /*userList.add(new User(1, "Random Rodelas", BitmapFactory.decodeResource(getResources(), R.mipmap.p2), "12:00"));
        userList.add(new User(2, "Homer Simpson", BitmapFactory.decodeResource(getResources(), R.mipmap.p1), "09:00"));
        userList.add(new User(3, "Leonel Messi", BitmapFactory.decodeResource(getResources(), R.mipmap.p3), "08:00"));
        userList.add(new User(1, "Random Rodelas", BitmapFactory.decodeResource(getResources(), R.mipmap.p2), "12:00"));
        userList.add(new User(2, "Homer Simpson", BitmapFactory.decodeResource(getResources(), R.mipmap.p1), "09:00"));
        userList.add(new User(3, "Leonel Messi", BitmapFactory.decodeResource(getResources(), R.mipmap.p3), "08:00"));
        userList.add(new User(1, "Random Rodelas", BitmapFactory.decodeResource(getResources(), R.mipmap.p2), "12:00"));
        userList.add(new User(2, "Homer Simpson", BitmapFactory.decodeResource(getResources(), R.mipmap.p1), "09:00"));
        userList.add(new User(3, "Leonel Messi", BitmapFactory.decodeResource(getResources(), R.mipmap.p3), "08:00"));
        userList.add(new User(1, "Random Rodelas", BitmapFactory.decodeResource(getResources(), R.mipmap.p2), "12:00"));
        userList.add(new User(2, "Homer Simpson", BitmapFactory.decodeResource(getResources(), R.mipmap.p1), "09:00"));
        userList.add(new User(3, "Leonel Messi", BitmapFactory.decodeResource(getResources(), R.mipmap.p3), "08:00"));*/

        recyclerView = view.findViewById(R.id.recyclerViewContactsMainFragment);
        mainMaterialToolbar = view.findViewById(R.id.topAppBarContactsFragment);
        searchToolbar = view.findViewById(R.id.topAppBarSearchContactFragment);

        setSearchToolbar();

        /*recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new ContactListAdapter(userList));*/



        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        FirestoreRecyclerOptions<User> options = new FirestoreRecyclerOptions.Builder<User>()
                .setQuery(db.getContactQuery(myAuthFirebase.getFirebaseUser().getUid()), User.class)
                .build();
        adapter = new ContactListFireStoreAdapter(options, getContext(), db);
        //adapter = new ContactListFirebaseAdapter(options, db, getContext(), myAuthFirebase.getFirebaseUser().getUid());
        recyclerView.setAdapter(adapter);

        mainMaterialToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavDirections navDirections = FragmentHomeDirections.actionFragmentHomeToContactFragment();
                Navigation.findNavController(v).popBackStack();
            }
        });

        mainMaterialToolbar.setOnMenuItemClickListener(new androidx.appcompat.widget.Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.search_main_toolbar:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            searchView.requestFocus();
                            circleReveal(R.id.topAppBarSearchContactFragment, 1, true, true);
                        }else {
                            searchToolbar.setVisibility(View.GONE);
                        }
                        return true;
                    case R.id.options:
                        NavDirections navDirections = FragmentHomeDirections.actionFragmentHomeToSettingsFragment();
                        NavHostFragment.findNavController(getParentFragment()).navigate(navDirections);
                    default:
                        return false;
                }
            }
        });


    }

    private void setSearchToolbar(){
        if (searchToolbar != null){
            searchToolbar.inflateMenu(R.menu.menu_toolbar_search);
            search_menu = searchToolbar.getMenu();

            searchToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        circleReveal(R.id.topAppBarSearchContactFragment, 1, true, false);
                    }else {
                        searchToolbar.setVisibility(View.GONE);
                    }
                }
            });
        }

        item_search = search_menu.findItem(R.id.actionFilterSearch);

        MenuItemCompat.setOnActionExpandListener(item_search, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    circleReveal(R.id.topAppBarSearchContactFragment,1,true,false);
                }
                else
                    searchToolbar.setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                return true;
            }
        });

        initSearchView();

    }

    private void initSearchView(){
        searchView = (SearchView) search_menu.findItem(R.id.actionFilterSearch).getActionView();

        int magId = getResources().getIdentifier("android:id/search_mag_icon", null, null);
        ImageView magImage = (ImageView) searchView.findViewById(magId);
        magImage.setLayoutParams(new LinearLayout.LayoutParams(0, 0));

        magImage.setVisibility(View.GONE);
        searchView.setQueryHint("Serach...");
        searchView.setIconifiedByDefault(false);

        try {
            Field mDrawable = SearchView.class.getDeclaredField("mIcon");
            mDrawable.setAccessible(true);
            Drawable drawable = (Drawable) mDrawable.get(searchView);
            drawable.setVisible(false,false);
            Log.d("prova", "si funciona ");
        } catch (Exception e) {
            Log.d("prova", "no funciona ");
            e.printStackTrace();
        }

    }

    @SuppressLint("PrivateResource")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void circleReveal(int viewId, int posFromRight, boolean containsOverFlow, final boolean isShow){
        final View view = getView().findViewById(viewId);

        int width=view.getWidth();

        if(posFromRight>0){
            width-=(posFromRight*getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material))-(getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material)/ 2);
        }
        if(containsOverFlow){
            width-=getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_overflow_material);
        }

        int cx = width;
        int cy = view.getHeight()/2;

        Animator anim;

        if(isShow){
            anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, (float)width);
        }else {
            anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, (float)width, 0);
        }

        anim.setDuration((long)400);

        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if(!isShow){
                    view.setVisibility(View.GONE);
                    mainMaterialToolbar.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });


        if (isShow) {
            mainMaterialToolbar.setVisibility(View.GONE);
            view.setVisibility(View.VISIBLE);
        }

        anim.start();
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}