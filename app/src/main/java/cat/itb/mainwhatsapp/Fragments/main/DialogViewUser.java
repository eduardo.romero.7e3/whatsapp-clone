package cat.itb.mainwhatsapp.Fragments.main;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

import cat.itb.mainwhatsapp.Model.User;
import cat.itb.mainwhatsapp.R;

public class DialogViewUser {
    private Context context;

    public DialogViewUser(Context context, User user) {
        this.context = context;
        initialize(user);
    }

    public void initialize(User user){

        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_ACTION_BAR);
        dialog.setContentView(R.layout.dialog_view_user);

        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(Objects.requireNonNull(dialog.getWindow()).getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(lp);

        ImageButton btnChat, btnCall, btnVideoCall, btnInfo;
        ImageView profile;
        TextView userName;

        btnChat = dialog.findViewById(R.id.btn_chat_dialog);
        btnCall = dialog.findViewById(R.id.btn_call_dialog);
        btnVideoCall = dialog.findViewById(R.id.btn_video_dialog);
        btnInfo = dialog.findViewById(R.id.btn_info_dialog);
        profile = dialog.findViewById(R.id.image_profile_dialog);
        userName = dialog.findViewById(R.id.tv_username_dialog);

        userName.setText(user.getName());
        //profile.setImageBitmap(user.getImage());

        dialog.show();
    }
}
